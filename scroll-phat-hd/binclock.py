#!/usr/bin/env python3

import scrollphathd as sp
from datetime import datetime
from time import sleep


def set(x, y, height, on):
    if on:
        sp.fill(1, x,y, 2,height)
    else:
        sp.fill(0.2, x,y, 2, height)


def set_sec(n, on):
    set(n*3, 5, 2, on)

def set_min(n, on):
    set(n*3, 2, 2, on)

def set_h(n, on):
    set(n*3, 0, 1, on)


def init():
    sp.clear()
    for i in range(0, 6):
        set_sec(i, False)
        set_min(i, False)
        if i < 5:
            set_h(i, False)


def run():
    now = datetime.now()

    h = now.hour
    m = now.minute
    s = now.second
    
    if run.lastSecond != s:
        run.lastSecond = s
        for i in range(0, 6):
            set_sec(i, s % 2 == 1)
            set_min(i, m % 2 == 1)
            if i < 5:
                set_h(i, h % 2 == 1)
            h = h // 2
            m = m // 2
            s = s // 2

        sp.show()

run.lastSecond = 61


init()
while True:
    run()
    sleep(0.1)

